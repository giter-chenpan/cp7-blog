
let express = require('express');
let app = express();
const MongoClient = require('mongodb').MongoClient; //数据库链接插件
const assert = require('assert');                   //程序断言


let url ='mongodb://localhost:27017/blog';
const dbName = 'blog';

app.all('*' , (req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*");
    next();
});

// 设置静态文件托管 当用户访问的url以/index开始，直接返回__dirname+'/cms'下的文件
app.use('/index', express.static(__dirname + '/cms'))



app.get('/',(req,res) => {
    res.end('get name')
});

//登陆后台管理系统
app.get('/login',(req,res) => {
    //请求参数的判定
    if(!(req.query.pwd && req.query.username) || req.query.pwd.length !== 6) {
        res.writeHead(500,{"Content-Type":"application/json"});
        res.end(JSON.stringify({code:500,data:'error',mess:'请输入六位数密码和正确的账号'}))
    }else{
        let user = req.query.username,
            pwd = req.query.pwd;
        //数据库链接
        MongoClient.connect(url,{useNewUrlParser: true}, (err,db) => {
            assert.equal(null, err);
            console.log("Connected successfully to server now --------" + new Date());

            const dbs = db.db(dbName);
            //数据插入
            // dbs.collection('user').insertOne({
            //     "_id" : "001",
            //     "username" : user,
            //     "pwd" : pwd
            // },(err) => {
            //     assert.equal(err, null);
            //     console.log("Inserted a document into the restaurants collection.");
            // })


            //数据查找
            const collection = dbs.collection('user');
            collection.find({'username':user}).toArray((err,docs) => {
                assert.equal(err, null);
                console.log("Found the following records");
                console.log(docs);
                if (!docs[0]){
                    res.writeHead(500,{"Content-Type":"application/json"});
                    res.end(JSON.stringify({code:500,data:'error',mess:'请输入正确的账号或密码'}))
                }else{
                    res.writeHead(200,{"Content-Type":"application/json"});
                    res.end(JSON.stringify({code:200,data:'success',mess:'登陆成功'}))
                }
            });
            db.close()
        });
    }
});

app.listen(8080, 'localhost' , () => {
    console.log('serve start!')
});